struct Universe {
    cells: Vec<Cell>,
}

impl Universe {
    fn new(cells: Vec<Cell>) -> Self {
        Self { cells }
    }

    fn alive_cells(self) -> Vec<Cell> {
        return self.cells
            .into_iter()
            .filter(|c|c.is_alive)
            .collect();
    }
}

#[derive(Debug, PartialEq)]
struct Cell {
    is_alive: bool,
    x: i64,
    y: i64,
}

impl Cell {
    fn new(is_alive: bool, x: i64, y: i64) -> Self {
        Self { is_alive, x, y }
    }
}

#[test]
fn test_universe_alive_cells() {
    let cells = vec!(
        Cell::new(false, 12, 13),
        Cell::new(true, 12, 14),
    );
    let universe = Universe::new(cells);
    assert_eq!(vec!(Cell::new(true, 12, 14)), universe.alive_cells());
}

#[test]
fn test_can_create_new_universe() {
    let cells = vec!(
        Cell::new(false, 12, 13),
        Cell::new(true, 12, 14),
    );
    Universe::new(cells);
}

#[test]
fn test_create_new_cell() {
    Cell::new(true, 2, 3);
}
